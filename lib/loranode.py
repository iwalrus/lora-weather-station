""" OTAA Node example compatible with the LoPy Nano Gateway """
import socket
import binascii
import struct
import time
import config
import gc
import pycom
from network import LoRa

class LoraConn(object):
    """ Provides methods for getting a connected lora socket """
    lora = None
    socket = None

    def __init__(self):
        self.lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868)


    def get_socket(self):
        # initialize LoRa in LORAWAN mode.
        # Please pick the region that matches where you are using the device:
        # Asia = LoRa.AS923
        # Australia = LoRa.AU915
        # Europe = LoRa.EU868
        # United States = LoRa.US915
        if not pycom.nvs_get('loraSaved'):
            print("no saved lora state")
            # create an OTA authentication params
            dev_eui = config.DEV_EUI
            app_eui = config.APP_EUI
            app_key = config.APP_KEY
            # set the 3 default channels to the same frequency (must be before sending the OTAA join request)
            self.lora.add_channel(0, frequency=config.LORA_FREQUENCY, dr_min=0, dr_max=5)
            self.lora.add_channel(1, frequency=config.LORA_FREQUENCY, dr_min=0, dr_max=5)
            self.lora.add_channel(2, frequency=config.LORA_FREQUENCY, dr_min=0, dr_max=5)

            # join a network using OTAA
            self.lora.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0, dr=config.LORA_NODE_DR)

            # wait until the module has joined the network
            while not self.lora.has_joined():
                time.sleep(2.5)
                print('Not joined yet...')

            # remove all the non-default channels
            for i in range(3, 16):
                self.lora.remove_channel(i)
        else:
            self.lora.nvram_restore()

        # create a LoRa socket
        s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

        # set the LoRaWAN data rate
        s.setsockopt(socket.SOL_LORA, socket.SO_DR, config.LORA_NODE_DR)

        # make the socket blocking
        s.setblocking(True)

        return s


