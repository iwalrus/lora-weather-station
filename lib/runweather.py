""" OTAA Node example compatible with the LoPy Nano Gateway """
import gc
import time
import machine
import loranode
import pycom
import config
from deepsleep import DeepSleep
from anemometer import Anemometer
from lpwan import minify
from uv import UVSensor
from bme280 import bme280
from bme280 import bme280_i2c


def run():
    """ Do stuff """

    # First of all, get a lora socket. By default this will
    # try to load saved connection data from NVRAM.
    loraconn = loranode.LoraConn()
    s = loraconn.get_socket()

    # Initialise our UV detector
    uvs = UVSensor(config.PIN_UV)

    # And our anemometer
    ane = Anemometer(config.PIN_ANEMOMETER)

    # And our temperature/pressure/humidity sensor
    bme280_i2c.set_default_i2c_address(config.ADDRESS_BME280)
    bme280_i2c.set_default_bus(config.BUS_BME280)
    bme280.setup()

    # We start in main.py when we wake from deepsleep, so this
    # loop only actually runs once, but it's handy to have
    # it here for testing.
    while True:
        try:
            # Get temperature/preasure/humidity
            tph = bme280.read_all()
        except OSError:
            # Maybe a transient I2C error - just try rebooting.
            machine.reset()

        # Read the UV sensor
        uv_level = uvs.read()

        # Measure wind speed - we sample for 2 seconds, 3 times
        # and average the result.
        samples = []
        print("about to sample")
        for _ in range(1, 3):
            print("sampling")
            samples.append(ane.sample(2))
        avg_wind_speed = sum(samples) / len(samples)

        # Use the highest of our wind samples as the 'gust' speed
        gust = max(samples)

        print("{}, {}, {}, {}, {}, {}".format(tph.temperature,
                                              tph.pressure,
                                              tph.humidity,
                                              avg_wind_speed,
                                              gust,
                                              uv_level))

        # Record our memory stats
        gc.collect()
        mem_free = gc.mem_free()

        # Pack it all into a byte array
        data = (minify(tph.temperature) +
                minify(tph.pressure) +
                minify(tph.humidity) +
                minify(avg_wind_speed) +
                minify(gust) +
                uv_level.to_bytes(2, 'little') +
                mem_free.to_bytes(2, 'little'))

        print('Sending {} bytes: {}'.format(len(data), data))
        try:
            s.send(data)
        except OSError: # try to recover!
            print("Lora down - trying to reconnect")
            pycom.nvs_set('loraSaved', 0)
            s = loraconn.get_socket()
            s.send(data)

        print('Finished sending.')

        # Make sure our connection data is saved
        loraconn.lora.nvram_save()
        pycom.nvs_set('loraSaved', 1)

        # Aaaand relax.
        ds = DeepSleep()
        ds.go_to_sleep(30)

        #rx, port = s.recvfrom(256)
        #if rx:
        #    print('Received: {}, on port: {}'.format(rx, port))