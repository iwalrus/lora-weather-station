def minify(val):
    """
    Takes a floating point value (designed for decimal minutes)
    and returns a bytes object that contains the uint32 representation
    of it
    """
    temp = int(val * 10000)
    b = temp.to_bytes(4,'little')
    return bytes([b[3], b[2], b[1], b[0]])
