""" UV Detector """
from machine import ADC

class UVSensor(object):
    """ UV detector """
    light = None


    def __init__(self, pin, attn=ADC.ATTN_0DB):
        adc = ADC()
        self.light = adc.channel(pin=pin, 
                                 attn=attn)

    
    def read(self):
        """ Read the UV light level """
        return self.light.value()
